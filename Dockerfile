FROM eclipse-temurin:17-alpine AS build
WORKDIR /code
COPY . .
RUN ./gradlew setup build
